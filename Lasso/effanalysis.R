# This is to analyze the sampling efficiency of algorithms RWM, Wall HMC, and Spherical HMC
# 
# Author: LANZI
###############################################################################

rm(list=ls())
set.seed(2013)
library(coda)
library(Hmisc)

problem <- c('Lasso_bayes','Lasso_whmc','Lasso_sph','Bridge_Q1.2_sph','Bridge_Q0.8_sph')
L <- length(problem)

Nsf =10

AP = matrix(0,L,Nsf)
s = matrix(NA,L,Nsf)
ESS = array(NA,c(L,3,Nsf))
MinESSs = matrix(NA,L,Nsf)


# analyze the efficiency
for(sf in 1:Nsf){
for(l in 1:L){
	f_name = list.files('./result/',paste(problem[l],'.*\\.RData',sep=''),recursive=T)
	load(paste('./result/',f_name,sep=''))
	
	s[l,sf] = Time[sf]/NSamp
	if(problem[l]=='Lasso_bayes') ess = pmin(effectiveSize(Samp[[1]][,,sf]),NSamp-NBurnIn) else
#	else if(problem[l]=='Lasso_whmc'){
		{ess = pmin(effectiveSize(Samp[[1]][,,sf]),NSamp-NBurnIn)
		AP[l,sf] = acpt[sf]}
#	} else{
#		ess = pmin(effectiveSize(ReSamp[,,sf]),NSamp-NBurnIn)
#		AP[l,sf] = acpt[sf]
#	}
	ESS[l,,sf] = c(min(ess),median(ess),max(ess))
	MinESSs[l,sf] = ESS[l,1,sf]/Time[sf]
	
	cat('Efficiency Stats of Problem ',problem[l], 'at shrinkage factor ',.1*sf, ' is:\n')
	cat('Acceptance Rate: ', AP[l,sf],'\n')
	cat('Time per iteration: ', s[l,sf],'\n')
	cat('Effective Sample Size (Min,Med,Max): ', ESS[l,,sf],'\n')
	cat('Time Normalized Efficiency: ', MinESSs[l,sf],'\n\n')
	
}

}


# plot the efficiency

op <- par(mar=c(3,3,2,1) + 0.1,oma=rep(0,4),mgp=c(1.5,0.5,0))

#for(l in 1:L){
#	errbar(.15*(l-1)+1:Nsf,ESS[l,2,],ESS[l,3,],ESS[l,1,],ylim=c(0,10000),xlab='Shrinkage Factor',ylab='Effective Sample Size',lty=l,lwd=1.4,col=l+1,errbar.col=l+1,add=(l!=1),xaxt='n')
#}
#axis(1,at=1:Nsf,labels=seq(0.1,1,by=.1))
#legend('bottomright',legend=c('Bayes Lasso','Spherical Lasso','q=1.2','q=0.8'),lty=1:L,lwd=1.4,col=1+1:L)


matplot(t(MinESSs),xlab='Shrinking Factor',ylab='Min(ESS)/s',type='l',lwd=1.4,col=1+1:L,xaxt='n')
axis(1,at=1:Nsf,labels=seq(0.1,1,by=.1))
legend('topleft',legend=c('Bayes Lasso','Wall Lasso','Spherical Lasso','Spherical Bridge Regression (q=1.2)','Spherical Bridge Regression (q=0.8)'),lty=1:L,lwd=1.4,col=1+1:L)

par(op)

