## This is to realize spherical HMC algorithm ##

# inputs:
#   current.q: initial state of q, |q|=1
#   U: =-log(density(q)), potential function of q
#   U.grad: gradient of U
#   eps: step size
#   L: number of leapfrogs
# outputs:
#   q: new state of beta
#   E: new potential energy
#   Ind: proposal acceptance indicator

SphHMC = function (current.q, U, eps=.2, L=5){
#    browser()
    D <- length(current.q)
    
    #initialization
    q = current.q
    
    # sample velocity
    v = rnorm(D) # standard multinormal
    v <- v - q*(t(q)%*%v) # force v to lie in tangent space of sphere
    
    # Evaluate potential and kinetic energies at start of trajectory
    current.E <- U(q) + .5*sum(v^2)
	
	randL = ceiling(L*runif(1))
    
    # Alternate full steps for position and momentum
    for (i in 1:randL){
	    
	    # Make a half step for velocity
	    v = v - eps/2 * U(q,d=T)
	    
	    # Make a full step for the position
	    q0 = q; v_nom = sqrt(sum(v^2))
	    q = q0*cos(v_nom*eps) + v/v_nom*sin(v_nom*eps)
	    v <- -q0*v_nom*sin(v_nom*eps) + v*cos(v_nom*eps)
	    
	    # Make last half step for velocity
	    v = v - eps/2 * U(q,d=T)
    }
    
    # Evaluate potential and kinetic energies at end of trajectory
    proposed.E <- U(q) + .5*sum(v^2)
    
    # Accept or reject the state at end of trajectory, returning either
    # the position at the end of the trajectory or the initial position
    logAP = -proposed.E + current.E
    
    if( is.finite(logAP)&(logAP>min(0,log(runif(1)))) ) return (list(q = q, Ind = 1))
    else return (list(q = current.q, Ind = 0))
    
}
