Spherical Hamiltonian Monte Carlo (SphHMC) is an HMC algorithm dedicated for 
efficiently sampling from probability distributions with norm constraints.
It maps the constrained domain to a sphere in a space with one extra dimension augmented.
The method handles the constraints implicitly by moving freely on the sphere 
generating proposals that remain within boundary when mapped back to the original space.
It has extensive applications in regression models with norm constraints (e.g., Lasso), 
probit models, many copula models, and Latent Dirichlet Allocation (LDA) models.

See a quick demo:
http://users.cms.caltech.edu/~slan/SphMC/animation/

This repo contains R codes for examples in the following paper:

Shiwei Lan, Bo Zhou, Babak Shahbaba
Spherical Hamiltonian Monte Carlo for Constrained Target Distributions
ICML 2014, Beijing, Proceedings of The 31st International Conference on Machine Learning, pp. 629-637, 2014
http://jmlr.org/proceedings/papers/v32/lan14.html

CopyRight: Shiwei Lan and Bo Zhou

Please cite the reference when using the codes, Thanks!

Shiwei Lan
9-18-2016