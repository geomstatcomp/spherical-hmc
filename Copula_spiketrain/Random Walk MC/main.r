
rm(list=ls(all=TRUE))
source( "sampler.latent.r" ) ## function to sample latent variables
source( "sampler.hyper.r" ) ## function to sample hyper parameters
source( "MCMC.r" ) ## Gibs sampler for copula-based gaussian process model for multiple spke trains.
source( "log.like.r" ) ## function to calculate the loglikelihood function
load("data.RData")
library( coda )


## Sampling

time <- 0:100/100
Niteration <- 3000
BurnIn <- 1000
w <- 1
m <- 10
epsilon <- 0.02

start <- proc.time()
set.seed(123456)
Sample_case1 <- GP_COPULAS( time, Y, Niteration, BurnIn, w, m, epsilon )
Time = proc.time()-start
Time.MCMC = Time[1]
Sample <- Sample_case1
save.image( "MCMC.RData" )


# Final Acceptance Rate
acpt = Sample$acpt/(Niteration-BurnIn)
cat('The Final Acceptance Rate: ',acpt,'\n')


## Caculating the est of parameters with the corresponding 95% posterior intervals
d <- dim(Y)[1]
n <- dim(Y)[2]
ZETA <- Sample$ZETA
PP <- Sample$PP[,d*BurnIn+1:(d*(Niteration-BurnIn))]
zeta <- ZETA

zeta.est <- matrix(0,nrow=d,ncol=d)
zeta.UCI <- matrix(0,nrow=d,ncol=d)
zeta.LCI <- matrix(0,nrow=d,ncol=d)
for(j in 1:d )
	for(k in 1:d ){
		zeta.est[j,k] <- mean( zeta[j,k,BurnIn:Niteration])
		zeta.LCI[j,k] <- sort( zeta[j,k,BurnIn:Niteration])[0.025*(Niteration-BurnIn)]
		zeta.UCI[j,k] <- sort( zeta[j,k,BurnIn:Niteration])[0.975*(Niteration-BurnIn)]
	}
zeta.est*upper.tri(zeta.est)
zeta.LCI*upper.tri(zeta.LCI)
zeta.UCI*upper.tri(zeta.UCI)


## function to plot the traceplots of the samples
library( coda )
plot.zeta <- function( zeta, i, j, d, Niteration, BurnIn ){
	zeta.ij <- zeta[i,j,BurnIn:Niteration]
	traceplot( mcmc(zeta.ij) )
}

plot.zeta(zeta,i=2,j=5,d,Niteration,BurnIn) ## i, j indexs neurons, d is the total number of neurons




