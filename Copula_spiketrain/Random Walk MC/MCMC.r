##
##### Function to sample hyperparameters based upon training dataset
##

GP_COPULAS <- function( time, Y, Niteration, BurnIn, w, m, epsilon ){
	
	n <- dim(Y)[2] # number of time bins
	D <- dim(Y)[1] # number of neurons
	N <- dim(Y)[3] # number of trials

	logeta <- matrix( 0, nrow=D, ncol=Niteration )
	logrho <- matrix( 0, nrow=D, ncol=Niteration )
	logalpha <- matrix( 0, nrow=D, ncol=Niteration )
	logJ2 <- matrix( 0, nrow=D, ncol=Niteration )
	
	acpt <- 0 # overall acceptance
	accp <- 0 # online acceptance

	UU <- matrix( rnorm( n*D, 0, 0.1 ), nrow=n )
	

	PP <- matrix( 0, nrow=n, ncol=D*Niteration )
	ZETA <- array( 0, c(D,D,Niteration) )
	DiffMat <- ( rep( time, n ) - rep( time, each=n ) )^2 ## used to calculate the covariance matrix

	for( i in 1:(Niteration-1) ){

    		if(i%%100==0){
        	cat('Iteration ',i,' completed!\n')
        	cat('Acceptance Rate: ', accp/100,'\n')
        	accp<-0
    		}

		#browser()
		print( i )
		for( j in 1:D ){

			## sample latent variables 
			C <- exp(logJ2[j,i])*diag(1,nrow=n,ncol=n)+exp(logeta[j,i])*exp(-matrix(DiffMat*exp(logrho[j,i]),nrow=n))+exp(logalpha[j,i]) ## covariance matrix
			L <- chol(C) ## Cholesky decomposition
			invL <- solve(L) ## Inverse of L
			invC <- invL%*%t(invL) ## Inverse of C
			pp <- exp(UU)/(1+exp(UU))
			UU[,j] <- sampler.u( UU[,j], Y, pp, L, invC, ind.neuron=j, zeta=ZETA[,,i], n, D, N, epsilon )
			PP[,i*D+j] <- exp(UU[,j])/(1+exp(UU[,j]))

			## sample hyper-parameters
			logeta[j,i+1] <- sampler.eta( UU[,j], logeta[j,i], logrho[j,i], logalpha[j,i], logJ2[j,i], n, w, m, DiffMat )
			logrho[j,i+1] <- sampler.rho( UU[,j], logeta[j,i+1], logrho[j,i], logalpha[j,i], logJ2[j,i], n, w, m, DiffMat )
			logalpha[j,i+1] <- sampler.alpha( UU[,j], logeta[j,i+1], logrho[j,i+1], logalpha[j,i], logJ2[j,i], n, w, m, DiffMat )
			logJ2[j,i+1] <- sampler.J2( UU[,j], logeta[j,i+1], logrho[j,i+1], logalpha[j,i+1], logJ2[j,i], n, w, m, DiffMat ) 
		}

		## sample copula parameters zeta
		pp <- exp(UU)/(1+exp(UU))
		ZETA[,,i+1] <- sampler.zeta( ZETA[,,i], Y, pp, n, D, N, epsilon )
		Ind <- as.numeric( ZETA[1,2,i]!=ZETA[1,2,i+1] )
    		accp <- accp + Ind
    		if(i>=BurnIn){
       		acpt <- acpt + Ind
    		}
	}
	
	out <- list( logeta, logrho, logalpha, logJ2, PP, ZETA, acpt )
	names( out ) <- c( "logeta", "logrho", "logalpha", "logJ2", "PP", "ZETA", "acpt" )
	return( out )
}
