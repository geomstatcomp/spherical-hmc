

rm(list=ls(all=TRUE))
source( "sampler.latent.r" ) # function to sample latent variables
source( "sampler.hyper.r" ) # function to sample hyper parameters
source( "MCMC.r" ) # function to do Gibbs sampling
source( "log.like.r" ) # function to calculating log likelihood
source( "WallHMC_diamond.R" )
load("data.RData")
library(gtools)
library( coda )



Niteration <- 3000
BurnIn <- 1000
w <- 1
m <- 10
epsilon <- 0.02


time <- 0:100/100
D <- 5 ## number of neurons
n <- length( time ) ## number of time bins
N <- dim(Y1)[3] ## number of trials



set.seed(1234567)
start <- proc.time()
Sample_case1 <- COPULA_HMC( time, Y , Niteration, BurnIn, w, m, epsilon )
Time = proc.time()-start
Time.WHMC = Time[1]
Sample <- Sample_case1
save.image( "WHMC.RData" )

# Final Acceptance Rate
acpt = Sample$acpt/(Niteration-BurnIn)
cat('The Final Acceptance Rate: ',acpt,'\n')
# Final wall hitting rate
hitrate = Sample$wallhits/(Niteration-BurnIn)
cat('The Wall hitting Rate: ',hitrate,'\n')

## calculating the estimates of copula parameters with the corresponding 95% posterior intervals
K <- D*(D-1)/2
for( i in 1:K)
	print( c(mean(Sample$Samp[,i]),sort(Sample$Samp[,i])[c(0.025*(Niteration-BurnIn),0.975*(Niteration-BurnIn))]) )


# plot the trace plots of copula parameters
library(coda)
pdf( "Beta.pdf" )
traceplot( mcmc(Sample$Beta[1,]) )
traceplot( mcmc(Sample$Beta[2,]) )
traceplot( mcmc(Sample$Beta[3,]) )
traceplot( mcmc(Sample$Beta[4,]) )
traceplot( mcmc(Sample$Beta[5,]) )
traceplot( mcmc(Sample$Beta[6,]) )
dev.off()