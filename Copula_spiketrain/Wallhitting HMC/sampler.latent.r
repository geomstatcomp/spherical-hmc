##
##### Sampler to sample latent variables using M-H algorithm
##

sampler.u <- function( U.before, Y, P, L, invC, ind.neuron, beta, epsilon ){
	## y.before is the latent variable befor each iteration, L is the Cholesky decomposition of prior covariance C and invC is inverse of C.
	## epsilon is some small constant, and n is number of subjects
	## proposal: y' = y + eLz where z is a vector of independent standard Guassian variables.

	## Calculate log-likelihood before sampling
	loglike.before.1 <- -U.data( beta, Y, P, d=F ) ## part I loglikelihood before sampling
	loglike.before.2 <- loglike.latent( yy=U.before, invC ) ## Part II loglikelihood

	## Sampler using M-H 	
	U.prop <- U.before + epsilon * t(L) %*% rnorm( n )
	P[,ind.neuron] <- exp( U.prop) / ( 1+exp(U.prop) )
	loglike.prop.1 <- -U.data( beta, Y, P, d=F ) ## part I loglikelihood of the proposal
	loglike.prop.2 <- loglike.latent( yy=U.prop, invC ) ## part II loglikelihood of the proposal

	## Calculate acceptance probability
	a <- min( 1, exp( loglike.prop.1 + loglike.prop.2 - loglike.before.1 - loglike.before.2 ) ) ## The proposal is symmetric
		
	## Decide whether the proposal is accepted or not 
	u <- runif(1,0,1)
	U.sample <- U.before
	if( u < a ){
		 U.sample <- U.prop
	}

	return( U.sample )
}


