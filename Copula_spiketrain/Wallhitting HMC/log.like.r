copula <- function(beta,y,qq,d=F){ # FGM copula of binary RVs with 2-interactions, q is failure rate
    #browser()
    mgcdf <- (qq^(1-y))*(y>=0) # (N,D) matrix
    prod2 <- apply(mgcdf,1,prod) # N vector
    if(d==F){
	  	prod1 <- apply(1-mgcdf,1,function(f){B=f%*%t(f); return(t(beta)%*%B[upper.tri(B)])}) # N vector
	    jtcdf = (1+prod1)*prod2 # N vector
	    if(any(jtcdf<0)) stop('Unmeaningful joint cdf!')
	    else return(jtcdf)
    }else
	    if(d==T){
	    	#browser()
	        prod3 <- apply(1-mgcdf,1,function(f){B=f%*%t(f); return(B[upper.tri(B)])}) # (D(D-1)/2, N) matrix
	        djtcdf <- t(prod3)*prod2 # (N, D(D-1)/2) matrix
	        return(djtcdf)
	    }
}

U <- function(beta,y,p,d=F){ # potential and its gradient in beta
	qq = 1-p
	y_prm = permutations(2,D,0:1,repeats.allowed=TRUE)
	#browser()
	pmf <- apply(y_prm,1,function(y_prm){y_diff=y-matrix(y_prm,N,D,byrow=T); return ((-1)^(sum(y_prm))*copula(beta,y_diff,qq,d=F))}) # (N, 2^D) matrix
	pmf <- rowSums(pmf) # N vector
	if(any(pmf<0)) stop('Unmeaningful pmf!')
	if(d==F){
		loglik = sum(log(pmf))
		logpri = -sum(beta^2)/(2*1*n)
		return(-(loglik+logpri))
      }else
	    if(d==T){
	    	dpmf <- apply(y_prm,1,function(y_prm){y_diff=y-matrix(y_prm,N,D,byrow=T); return ((-1)^(sum(y_prm))*copula(beta,y_diff,qq,T))}) # (N*D(D-1)/2, 2^D) matrix
	        dpmf <- matrix(rowSums(dpmf),N,D*(D-1)/2) # (N, D(D-1)/2) matrix
	        dloglik <- colSums(dpmf/pmf) # D(D-1)/2 vector, same size as beta
	        dlogpri <- -beta/(1*n)
	        return(-(dloglik+dlogpri))
	    }else stop('Wrong choice!')
}

U.data <- function(beta,Y,P,d=F){ # potential and its gradient in beta
	
	if(d==F){
		rslt <- 0
		for( ii in 1:n){
			rslt <- rslt + U(beta,y=t(Y[,ii,]),p=matrix(rep(P[ii,],N),nrow=N,byrow=TRUE),d=F)
		}
		return( rslt )
    }else
	    if(d==T){	    	
			rslt <- rep(0,D*(D-1)/2)
			for( ii in 1:n){
				rslt <- rslt + U(beta,y=t(Y[,ii,]),p=matrix(rep(P[ii,],N),nrow=N,byrow=TRUE),d=T)
			}
			return( rslt )
	    }else stop('Wrong choice!')
}


loglike.latent <- function( yy, invC ){	
	## log likelihood of the latent variables
	## log-likelihood=-1/2*y^t * invC * y
	
	log.like <- -0.5 * t(yy) %*% invC %*% yy
	
	return( log.like )
}

## log-likelihood of hyperparameters

loglike.hyper <- function( yy, log.eta, log.rho, log.alpha, log.J2, n, DiffMat ){
	## loglikelihood of the hyperparameters
	## yy is latent variables, Diffmat is a matrix used to calculate covariance matrix
	## log.eta, log.rho, log.alpha, log.J2 are hyperparameters, and n is number of subjects
		
	## normal prior for log( rho^2 ) ~ N(0,3)
	log.prior <- - 0.5*log.rho^2/3^2 - 0.5*log.eta^2/3^2 - 0.5*log.alpha^2/3^2 - 0.5*log.J2^2/3^2

	## Covariance Matrix
	CC <- exp(log.J2) * diag(1, nrow=n, ncol=n ) + exp(log.eta) * exp( -matrix( DiffMat * exp(log.rho), nrow=n ) ) + exp(log.alpha) 

	LL <- chol(CC) ## Cholesky decomposition
	logdetC <- 2 * sum( log(diag(LL)) )
	invL <- solve(LL) ## Inverse of L
	## invC = invL%*%t(invL) ## Inverse of C
	invL.y <- t(invL) %*% yy

	## log-likelihood L=-n/2log(2pi)-1/2log(det(C))-1/2y^TC^(-1)y
	log.like <- -0.5 * logdetC - 0.5 * t(invL.y) %*% invL.y

	## log-posterior of hyperparameters
	log.post <- log.prior + log.like
	return( log.post )
}


