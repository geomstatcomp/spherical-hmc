##
##### Function to do sampling
##

COPULA_WHMC <- function( time, Y, Niteration, BurnIn, w, m, epsilon ){
	
	n <- dim(Y)[2] # number of time bins
 	D <- dim(Y)[1] # number of neurons
	N <- dim(Y)[3] # number of Trials
	
	logeta <- matrix( 0, nrow=D, ncol=Niteration )
	logrho <- matrix( 0, nrow=D, ncol=Niteration )
	logalpha <- matrix( 0, nrow=D, ncol=Niteration )
	logJ2 <- matrix( 0, nrow=D, ncol=Niteration )
	UU <- matrix( rnorm( n*D, -1.5, 0.1 ), nrow=n )
	PP <- array( 0, c(n,D,Niteration) )

	## Spherical HMC setting
	TrjLength <- 0.6/D
	NLeap <- 10
	Stepsz <- TrjLength/NLeap

	## storage
	K <- D*(D-1)/2
	NSamp <- Niteration
	NBurnIn <- BurnIn
	Samp <- matrix(NA,NSamp-NBurnIn,K)
	acpt <- 0 # overall acceptance
	accp <- 0 # online acceptance
	wallhits = 0 # count the number of Wall hitting

	## Initialization
	beta <- rep(0,K)
	Beta <- matrix(0,nrow=K,ncol=Niteration)

	DiffMat <- ( rep( time, n ) - rep( time, each=n ) )^2 ## used to calculate the covariance matrix

	#browser()
	for( i in 1:(Niteration-1) ){

	    	# display online acceptance rate per 100 iteration
    		if(i%%100==0){
        	cat('Iteration ',i,' completed!\n')
        	cat('Acceptance Rate: ', accp/100,'\n')
        	accp<-0
    		}

		print( i )
		for( j in 1:D ){

			## sample latent variables 
			C <- exp(logJ2[j,i])*diag(1,nrow=n,ncol=n)+exp(logeta[j,i])*exp(-matrix(DiffMat*exp(logrho[j,i]),nrow=n))+exp(logalpha[j,i]) ## covariance matrix
			L <- chol(C) ## Cholesky decomposition
			invL <- solve(L) ## Inverse of L
			invC <- invL%*%t(invL) ## Inverse of C
			P <- exp(UU)/(1+exp(UU))
			UU[,j] <- sampler.u( UU[,j], Y, P, L, invC, ind.neuron=j, beta, epsilon )
			PP[,j,i+1] <- exp(UU[,j])/(1+exp(UU[,j]))

			## sample hyper-parameters
			logeta[j,i+1] <- sampler.eta( UU[,j], logeta[j,i], logrho[j,i], logalpha[j,i], logJ2[j,i], n, w, m, DiffMat )
			logrho[j,i+1] <- sampler.rho( UU[,j], logeta[j,i+1], logrho[j,i], logalpha[j,i], logJ2[j,i], n, w, m, DiffMat )
			logalpha[j,i+1] <- sampler.alpha( UU[,j], logeta[j,i+1], logrho[j,i+1], logalpha[j,i], logJ2[j,i], n, w, m, DiffMat )
			logJ2[j,i+1] <- sampler.J2( UU[,j], logeta[j,i+1], logrho[j,i+1], logalpha[j,i+1], logJ2[j,i], n, w, m, DiffMat ) 
		}

		## sample latent variables zeta
 		# Use Spherical HMC to get sample theta
    		samp <- WalHMC_diamond(beta, function(beta,d=F)U.data(beta,Y,PP[,,i+1],d=d), Stepsz, NLeap )
        	Beta[,i+1] <- beta <- samp$q
    		accp <- accp + samp$Ind
    
    		if(i==NBurnIn) cat('Burn in completed!\n')
    
    		# save sample beta
    		if(i>=NBurnIn){
        		Samp[i+1-NBurnIn,] = beta
       		acpt <- acpt + samp$Ind
			wallhits = wallhits + samp$wallhit
    		}



	}
	
	out <- list( logeta, logrho, logalpha, logJ2, PP, Samp, wallhits, acpt, Beta )
	names( out ) <- c( "logeta", "logrho", "logalpha", "logJ2", "PP", "Samp", "wallhits", "acpt", "Beta" )
	return( out )
}
