rm(list=ls(all=TRUE))
source( "sampler.latent.r" ) ## function to sample latent variables
source( "sampler.hyper.r" ) ## function to sample hyper parameters
source( "SphHMC.r" ) ## function to do spherical HMC 
source( "MCMC.r" ) ## function to do Gibbs samping
source( "log.like.r" ) ## function to calculate loglikelihood function
load("data.RData")

time <- 0:100/100
Niteration <- 300
BurnIn <- 1000
w <- 1
m <- 10
epsilon <- 0.02

D <- 5 ## number of neurons
n <- length( time ) ## number of time bins
N <- dim(Y)[3]  ## number of trials



set.seed(1234567)
start <- proc.time()
Sample_case1 <- COPULA_SHMC( time, Y , Niteration, BurnIn, w, m, epsilon )
Time = proc.time()-start
Time.SHMC = Time[1]
Sample <- Sample_case1
save.image( "SHMC.RData" )


# Final Acceptance Rate
acpt = Sample$acpt/(Niteration-BurnIn)
cat('The Final Acceptance Rate: ',acpt,'\n')

# Resample
ReSamp_id = sample(Niteration-BurnIn,Niteration-BurnIn,replace=T,prob=Sample$wt)
ReSamp = Sample$Samp[ReSamp_id,]
# Calculating the estimates of the copula parameters with the corresponding 95% posterior intervals
K <- D*(D-1)/2
for( i in 1:K)
	print( c(mean(ReSamp[,i]),sort(ReSamp[,i])[c(0.025*(Niteration-BurnIn),0.975*(Niteration-BurnIn))]) )


# plot the trace plots of the copula parameters
library( coda )
pdf( "Beta.pdf" )
traceplot( mcmc(Sample$Beta[1,]) )
traceplot( mcmc(Sample$Beta[2,]) )
traceplot( mcmc(Sample$Beta[3,]) )
traceplot( mcmc(Sample$Beta[4,]) )
traceplot( mcmc(Sample$Beta[5,]) )
traceplot( mcmc(Sample$Beta[6,]) )
dev.off()
