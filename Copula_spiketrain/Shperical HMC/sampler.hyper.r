##
##### Sampler to sample hyperparameters
##

sampler.eta <- function( yy, log.eta, log.rho, log.alpha, log.J2, n, w, m, DiffMat ){
	## Using slice sampler to sample the hyperparameters
	## yy is the latent variable

	z <- loglike.hyper( yy, log.eta, log.rho, log.alpha, log.J2, n, DiffMat ) - rexp( 1 )

	## Stepping out
	u <- runif(1)
	L <- log.eta - w * u
	R <- L + w
	v <- runif( 1 )
	J <- floor( m * v )
	K <- ( m - 1 ) - J

	while ( J>0 && z < loglike.hyper( yy, L, log.rho, log.alpha, log.J2, n, DiffMat ) ){
		L <- L - w	
		J <- J - 1
	}
	while ( K>0 && z < loglike.hyper( yy, R, log.rho, log.alpha, log.J2, n, DiffMat ) ){
		R <- R + w
		K <- K - 1
	}

	## Shrinkage to obtain a new sample
	u <- runif(1)
	newParam <- L + u * ( R - L )

	while ( z > loglike.hyper( yy, newParam, log.rho, log.alpha, log.J2, n, DiffMat ) ){
		if ( newParam < log.eta ) {
			L <- newParam
		}else{
			R <- newParam
		}
		u <- runif(1)
		newParam <- L + u*(R-L)
	}
	
	return( newParam )
}

sampler.rho <- function( yy, log.eta, log.rho, log.alpha, log.J2, n, w, m, DiffMat ){
	## Using slice sampler to sample the hyperparameters
	## yy is the latent variable

	z <- loglike.hyper( yy, log.eta, log.rho, log.alpha, log.J2, n, DiffMat ) - rexp( 1 )

	## Stepping out
	u <- runif(1)
	L <- log.rho - w * u
	R <- L + w
	v <- runif( 1 )
	J <- floor( m * v )
	K <- ( m - 1 ) - J

	while ( J>0 && z < loglike.hyper( yy, log.eta, L, log.alpha, log.J2, n, DiffMat ) ){
		L <- L - w	
		J <- J - 1
	}
	while ( K>0 && z < loglike.hyper( yy, log.eta, R, log.alpha, log.J2, n, DiffMat ) ){
		R <- R + w
		K <- K - 1
	}

	## Shrinkage to obtain a new sample
	u <- runif(1)
	newParam <- L + u * ( R - L )

	while ( z > loglike.hyper( yy, log.eta, newParam, log.alpha, log.J2, n, DiffMat ) ){
		if ( newParam < log.rho ) {
			L <- newParam
		}else{
			R <- newParam
		}
		u <- runif(1)
		newParam <- L + u*(R-L)
	}
	
	return( newParam )
}

sampler.alpha <- function( yy, log.eta, log.rho, log.alpha, log.J2, n, w, m, DiffMat ){
	## Using slice sampler to sample the hyperparameters
	## yy is the latent variable
	z <- loglike.hyper( yy, log.eta, log.rho, log.alpha, log.J2, n, DiffMat ) - rexp( 1 )

	## Stepping out
	u <- runif(1)
	L <- log.alpha - w * u
	R <- L + w
	v <- runif( 1 )
	J <- floor( m * v )
	K <- ( m - 1 ) - J

	while ( J>0 && z < loglike.hyper( yy, log.eta, log.rho, L, log.J2, n, DiffMat ) ){
		L <- L - w	
		J <- J - 1
	}
	while ( K>0 && z < loglike.hyper( yy, log.eta, log.rho, R, log.J2, n, DiffMat ) ){
		R <- R + w
		K <- K - 1
	}

	## Shrinkage to obtain a new sample
	u <- runif(1)
	newParam <- L + u * ( R - L )

	while ( z > loglike.hyper( yy, log.eta, log.rho, newParam, log.J2, n, DiffMat ) ){
		if ( newParam < log.alpha ) {
			L <- newParam
		}else{
			R <- newParam
		}
		u <- runif(1)
		newParam <- L + u*(R-L)
	}
	
	return( newParam )
}


sampler.J2 <- function( yy, log.eta, log.rho, log.alpha, log.J2, n, w, m, DiffMat ){
	## Using slice sampler to sample the hyperparameters
	## yy is the latent variable

	z <- loglike.hyper( yy, log.eta, log.rho, log.alpha, log.J2, n, DiffMat ) - rexp( 1 )

	## Stepping out
	u <- runif(1)
	L <- log.J2 - w * u
	R <- L + w
	v <- runif( 1 )
	J <- floor( m * v )
	K <- ( m - 1 ) - J

	while ( J>0 && z < loglike.hyper( yy, log.eta, log.rho, log.alpha, L, n, DiffMat ) ){
		L <- L - w	
		J <- J - 1
	}
	while ( K>0 && z < loglike.hyper( yy, log.eta, log.rho, log.alpha, R, n, DiffMat ) ){
		R <- R + w
		K <- K - 1
	}

	## Shrinkage to obtain a new sample
	u <- runif(1)
	newParam <- L + u * ( R - L )

	while ( z > loglike.hyper( yy, log.eta, log.rho, log.alpha, newParam, n, DiffMat ) ){
		if ( newParam < log.J2 ) {
			L <- newParam
		}else{
			R <- newParam
		}
		u <- runif(1)
		newParam <- L + u*(R-L)
	}
	
	return( newParam )
}



